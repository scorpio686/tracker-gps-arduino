# Tracker GPS Arduino

#Matériel utilisé actuellement :

- Arduino Mega 2560
- Module GPS NEO-6M : https://www.az-delivery.de/fr/products/neo-6m-gps-modul?_pos=9&_sid=86e67d81a&_ss=r
- Module ethernet ENC28J60 : https://www.az-delivery.de/fr/products/enc28j60-netzwerkmodul?_pos=1&_sid=e1bfded26&_ss=r


#Câblage :

    Module Ethernet :

        Arduino	     Module
        digital 12	     SO
        digital 11	     ST
        digital 13	     SCK
        digital 8	     CS
        5V	     5V
        Gnd	     GND


    Module GPS :

        Arduino     Module
        RX1 (19)    TX
        TX1 (18)    RX
        3.3v        VCC
        GND         GND
